#
# Be sure to run `pod lib lint StockCommonUI.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#
Pod::Spec.new do |s|
  s.name             = 'StockExpr'
  s.version          = '0.1.0'
  s.summary          = 'A short description of StockExpr.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://github.com/zhu.jialai@gmail.com/StockCommonUI'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'zhu.jialai@gmail.com' => 'zhujialai@icloud.com' }
  s.source           = { :git => 'git@gitlab.com:wellcomez/tinyexpr.git', :tag => 'assignzerocopy' }
  # pod 'StockChart', :path => './'
  # s.source           = { :git => 'git@gitlab.com:wellcomez/tinyexpr.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '11.0'
  s.osx.deployment_target = '10.10'
  s.ios.pod_target_xcconfig = { 'PRODUCT_BUNDLE_IDENTIFIER' => 'com.ll.zenstockexpr' }
  s.osx.pod_target_xcconfig = { 'PRODUCT_BUNDLE_IDENTIFIER' => 'com.ll.zenstockexpr' }
  s.pod_target_xcconfig = {
      'CLANG_CXX_LANGUAGE_STANDARD' => 'c++11',
      # 'CLANG_CXX_LANGUAGE_STANDARD' => 'compiler-default',
      'CLANG_CXX_LIBRARY' => 'libc++'
  }
  s.libraries = 'c++'
#   s.xcconfig = {
#     'CLANG_CXX_LANGUAGE_STANDARD' => 'c++11',
#     'CLANG_CXX_LIBRARY' => 'libc++'
# }
  # s.source_files = 'StockChart.h'
  s.source_files = 'Expr/*'
  # s.public_header_files = 'Expr/expr.h'
end
